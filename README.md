### HANGMAN ###

* HANGMAN browser game
* 0.0.2

### About ###

>Hangman game.

* maintain data after server/client reload
* send SMS
* P2P browser communication
* global statistics page


- HTML5
- JavaScript
- NodeJS
- Bootstrap
- SMS NEXMO API

### Set up ###
```sh
$ cd  src
$ npm install
$ node server.js
```
* point a browser (chrome) to http://localhost:8080

### Author ###

* Kostas Kapetanakis

![2015-12-31 00_25_49-Start.png](https://bitbucket.org/repo/y6Rzgo/images/3009464578-2015-12-31%2000_25_49-Start.png)